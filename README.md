All Readmes should follow a similar format when it comes to how your content is structured, which we'll talk about more below. There are also some technical markdown expectations that we have as well. 

## Readme Outline:


# `#` Title 

( The reading's name should be a descriptive phrase about its content. )

## `##` Overview

A 1-2 sentence summary of what will be covered in the Reading. (In some cases if you feel this isn't necessary, then you can remove this.)

## `##` Objectives

 1. Every reading should have a list of learning objectives that will describe what the student should be able to do or show after completing the lesson. Typically we will provide you with the learning objectives already.
 2. It should be a numbered list, in most cases with around 3 to 7 items.
 3. It should be written in the present-imperative tense. Overusing the verbs "learn", "recognize", "know", and "understand" should be avoided (e.g. good: "Run the test suite" vs bad: "Learn to run a test" or "Running tests." or "Know how to run a test").

## `##` Exposition

Remember to create a narrative that has a beginning, middle, and end. More on this in upcoming lessons.

But for now, make sure you explain why we are learning what we are learning. Give a real-world or pseudo-real-world example of using the technology/skill being discussed or show the pain of solving the problem using only the knowledge students have up to this point. 

Discuss the topic of the reading. Break into sections and sub-sections as appropriate to help with the reader's orientation with the material and future referencing.

Be opinionated! There are many ways to solve a problem with code and there are many ways to implement certain technologies, methods, etc. There are however, *best* ways to utilize these technologies, methods, techniques and we should be clear about that when we present multiple options. 

## `##` Resources

This is an optional section, but list and link to resources, articles, and websites that you think will be beneficial for additional reading.

## Some Readme Examples:

  * [Javascript- Intro to Data Types](https://github.com/learn-co-curriculum/javascript-intro-to-data-types)
	
  * [Swift- Functions](https://github.com/learn-co-curriculum/swift-functions-readme)
	
<p data-visibility='hidden'>View <a href='https://learn.co/lessons/readme-template' title='Readme Template.'>Readme Template.</p>


